# Filter composer

Filter composer dialog of the Adblock Plus browser extension.

The filter composer is used to hide a selected element or block requests that are related to the selected element.

![](../../res/abp/composer/composer.png)

Title: Same as [menu item label](#menu-item)

1. [Menu item](#menu-item)
1. [Header](#header)
1. [Filter area](#filter-area)
1. [Footer](#footer)

## Menu item

Label: `Block element`

Menu items for opening the dialog can be found in the following places:

1. [Bubble UI](bubble-ui.md#block-element)
1. Context menu

Clicking on the menu item let's the user select an element from the web page for which filters should be created.

## Header

1. [Adblock Plus logo](#assets)
2. `Add filter(s)?`

## Filter area

User-editable text input field that contains filters that were suggested by the extension to match the selected element. Stretches to occupy any available space.

The area should be focused when opening the dialog. It is disabled and shows placeholder text `Loading...` until it gets populated with filters.

## Footer

1. [Add button](#add-button)
2. [Cancel button](#cancel-button)

### Add button

Label: `Add`

Validates filters from the [Filter area](#filter-area), adds them to the extension and closes the dialog. This behavior can also be triggered by pressing `Enter` while the focus is outside the [Filter area](#filter-area).

The button is disabled until the [Filter area](#filter-area) gets populated with filters.

Errors during validation are displayed as a modal dialog.

### Cancel button

Label: `Cancel`

Closes the dialog. Can also be triggered by pressing `Escape`.

## Assets

| Name | Asset |
|-|-|
| ABP_Logo_Outline.svg | ![](../../res/abp/options-page/assets/ABP_Logo_Outline.svg) |
