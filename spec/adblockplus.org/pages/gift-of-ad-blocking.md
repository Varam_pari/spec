# "Gift of ad blocking" holiday campaign

## Previews

- [Desktop](/res/adblockplus.org/screenshots/goab-desktop.png)
- [Tablet (portrait)](/res/adblockplus.org/screenshots/goab-tablets.png)
- [Mobile](/res/adblockplus.org/screenshots/goab-mobiles.png)

## Page information

| Key | Value |
| :-- | :-- |
| Locales | `EN`  |
| URL | `gift-of-ad-blocking` |

## Meta data

| Key | Value |
| :-- | :-- |
| title | Give the gift of ad blocking this Christmas |
| description | A gift that keeps on giving, at times when you least expect it. Get and give Adblock Plus this holiday season. |
| og:title | Give the gift of ad blocking this Christmas |
| og:description | A gift that keeps on giving, at times when you least expect it. Get and give Adblock Plus this holiday season. |
| og:image | [Link to image](/res/adblockplus.org/static/gift-of-ad-blocking/goab-fb-share.png) |
| noheading | true |

## Content

**Important detail:** The page doesn't have a navigation menu at the top. This is a specific requirement from our Marketing team in order to keep the user engaged with the content on the page.

### Hero section

Asset(s):

- Background
  - [Desktop, tablets](/res/adblockplus.org/static/gift-of-ad-blocking/goab-hero-background.png)
  - [Mobiles](/res/adblockplus.org/static/gift-of-ad-blocking/goab-hero-background-mobiles.png)
- Facebook icon
  - [SVG](/res/adblockplus.org/static/icon-facebook-grey.svg)
  - [PNG](/res/adblockplus.org/static/icon-facebook-grey.png)
  - [PNG@2x](/res/adblockplus.org/static/icon-facebook-grey@2x.png)
- Twitter icon
  - [SVG](/res/adblockplus.org/static/icon-twitter-grey.svg)
  - [PNG](/res/adblockplus.org/static/icon-twitter-grey.png)
  - [PNG@2x](/res/adblockplus.org/static/icon-twitter-grey@2x.png)

```
# The gift of ad blocking

The holiday season is magical, but it can be a little... hectic. **Adblock Plus** can’t help you with everything, but it makes the perfect gift for a better Internet.

[See Stories Below](#next-slide)

[Share on Facebook](icon-facebook-grey.svg)

[Share on Twitter](icon-twitter-grey.svg)
```

When a user clicks on the `See Stories Below` button, the page automatically scrolls to the [next section](#photo-story-section).

#### Social sharing 2 click solution

The social sharing buttons require 2 clicks - for privacy reasons - before sending any information over to a 3rd party.

On the first click, the user can see a tooltip appear on the corresponding social button. Another click is required to enable the social sharing functionality. See below the content of the tooltip.

##### Privacy text for Facebook sharing

```
Click again to share this. Please note that by sharing this, some personal data is transferred to Facebook. For more information, please review Facebook’s [Data Policy](https://www.facebook.com/privacy/explanation).
```

##### Privacy text for Twitter sharing

```
Click again to share this. Please note that by sharing this, some personal data is transferred to Twitter. For more information, please review Twitter’s [Privacy Policy](https://twitter.com/en/privacy).
```

#### Social sharing message

After user clicks twice to enable social sharing, the buttons will open the corresponding platform sharer in a new tab.

##### Facebook sharing

Users are able to share the URL of the landing page, `https://www.adblockplus.org/en/gift-of-ad-blocking`, on Facebook. We share the URL of the landing page instead of the domain `www.giftofadblocking.com` because the latter doesn't have any meta information that can appear on Facebook (title, description, image).

```
https://www.facebook.com/sharer/sharer.php?u=http%3A//www.adblockplus.org/en/gift-of-ad-blocking
```

##### Twitter sharing

Users are able to share the message `See how Adblock Plus sprinkles a little holiday magic on the web: www.giftofadblocking.com`.

```
https://twitter.com/home?status=See%20how%20Adblock%20Plus%20sprinkles%20a%20little%20holiday%20magic%20on%20the%20web%3A%20www.giftofadblocking.com
```

### Photo story section

The campaign revolves around 4 different holiday stories that the user might identify with.

In order for users to check all of these out, this section of the page allows easy tab navigation in order to browse through all the stories.

[See preview >](/res/adblockplus.org/screenshots/goab-photo-story-section.png)

#### Content

```
## See how Adblock Plus sprinkles a little holiday magic on the web

Click on a thumbnail to see the whole photo-story!
```

#### Functionality

In order for the user to browse and select which photo story to see, the section contains a tab navigation menu at the top.

Depending on which tab is active, the user can see the content for a corresponding tab below the tab menu. Depending on a parameter in the URL, we show the user a specific tab as active:

| Parameter | Tab to show |
| :-- | :-- |
| `utm_content=the_skateboard` | [The skateboard](#the-skateboard) |
| `utm_content=the_party` | [The party](#the-party) |
| `utm_content=the_ring` | [The ring](#the-ring) |
| `utm_content=the_kitchen` | [The kitchen](#the-kitchen) |

If the user entered the page without any of these parameters in the URL, by default we show the user the first tab ([the skateboard](#the-skateboard)) as active.

#### Tabs

##### Tab menu

Asset(s):

- [The skateboard - PNG](/res/adblockplus.org/static/gift-of-ad-blocking/thumbnail-skateboard.png)
- [The skateboard - PNG@2x](/res/adblockplus.org/static/gift-of-ad-blocking/thumbnail-skateboard@2x.png)
- [The party - PNG](/res/adblockplus.org/static/gift-of-ad-blocking/thumbnail-party.png)
- [The party - PNG@2x](/res/adblockplus.org/static/gift-of-ad-blocking/thumbnail-party@2x.png)
- [The ring - PNG](/res/adblockplus.org/static/gift-of-ad-blocking/thumbnail-ring.png)
- [The ring - PNG@2x](/res/adblockplus.org/static/gift-of-ad-blocking/thumbnail-ring@2x.png)
- [The kitchen - PNG](/res/adblockplus.org/static/gift-of-ad-blocking/thumbnail-kitchen.png)
- [The kitchen - PNG@2x](/res/adblockplus.org/static/gift-of-ad-blocking/thumbnail-kitchen@2x.png)

[See preview >](/res/adblockplus.org/screenshots/goab-tab-navigation.png)

As mentioned above, the user should easily navigate through the stories using the tab menu. Clicking on each of the tab menu items  will show the corresponding tab content (see below).

##### Tab content

Each of the tabs contains a different slider, each telling a different photo story. Each slide in the slider (what a mouthful!) includes an image and a message beneath it. The user should be able to easily navigate through the different slides via the navigation arrows and buttons.

[See preview >](/res/adblockplus.org/screenshots/goab-tab-content.png)

Asset(s):

- Navigation arrows
  - [Arrow left SVG](/res/adblockplus.org/static/arrow-left-white.svg)
  - [Arrow left PNG](/res/adblockplus.org/static/arrow-left-white.png)
  - [Arrow left PNG@2x](/res/adblockplus.org/static/arrow-left-white@2x.png)
  - [Arrow right SVG](/res/adblockplus.org/static/arrow-right-white.svg)
  - [Arrow right PNG](/res/adblockplus.org/static/arrow-right-white.png)
  - [Arrow right PNG@2x](/res/adblockplus.org/static/arrow-right-white@2x.png)
- Navigation bullets (just in case)
  - [Active bullet SVG](/res/adblockplus.org/static/slider-nav-active.svg)
  - [Active bullet PNG](/res/adblockplus.org/static/slider-nav-active.png)
  - [Active bullet PNG@2x](/res/adblockplus.org/static/slider-nav-active@2x.png)
  - [Default bullet SVG](/res/adblockplus.org/static/slider-nav.svg)
  - [Default bullet PNG](/res/adblockplus.org/static/slider-nav.png)
  - [Default bullet PNG@2x](/res/adblockplus.org/static/slider-nav@2x.png)

Under each tab menu item, a short title is present:

| Tab menu | Title |
| :-- | :-- |
| [The skateboard](#the-skateboard) | `The Best Present` |
| [The party](#the-party) | `Party, interrupted` |
| [The ring](#the-ring) | `A Holiday Surprise` |
| [The kitchen](#the-kitchen) | `The Missing Ingredient` |

###### The skateboard

Asset(s):

- [Slide 1 image](/res/adblockplus.org/static/gift-of-ad-blocking/skateboard-slide-1.jpg)
- [Slide 2 image](/res/adblockplus.org/static/gift-of-ad-blocking/skateboard-slide-2.jpg)
- [Slide 3 image](/res/adblockplus.org/static/gift-of-ad-blocking/skateboard-slide-3.jpg)

```
[Mother puts helmet on her child](skateboard-slide-1.jpg)
You’d do anything to keep your kids safe

[Mother and son browsing online on laptop](skateboard-slide-2.jpg)
But they need a little independence

[Mother and son with skateboard in the park](skateboard-slide-3.jpg)
This year, gift yourself peace of mind with Adblock Plus
```

###### The party

Asset(s):

- [Slide 1 image](/res/adblockplus.org/static/gift-of-ad-blocking/party-slide-1.jpg)
- [Slide 2 image](/res/adblockplus.org/static/gift-of-ad-blocking/party-slide-2.jpg)
- [Slide 3 image](/res/adblockplus.org/static/gift-of-ad-blocking/party-slide-3.jpg)
- [Slide 4 image](/res/adblockplus.org/static/gift-of-ad-blocking/party-slide-4.jpg)

```
[Young people at holiday party](party-slide-1.jpg)
Music, conversation and drinks!

[Young people clinking glasses at holiday party ](party-slide-2.jpg)
Cheers!

[Young people interrupted during holiday party](party-slide-3.jpg)
*record scratch*

[Get Adblock Plus](party-slide-4.jpg)
Don’t let ads ruin the party. Get Adblock Plus
```

**Note:** Text in 2nd slide is not italic, the `*` are meant to be displayed to the user.

###### The ring

Asset(s):

- [Slide 1 image](/res/adblockplus.org/static/gift-of-ad-blocking/ring-slide-1.jpg)
- [Slide 2 image](/res/adblockplus.org/static/gift-of-ad-blocking/ring-slide-2.jpg)
- [Slide 3 image](/res/adblockplus.org/static/gift-of-ad-blocking/ring-slide-3.jpg)
- [Slide 4 image](/res/adblockplus.org/static/gift-of-ad-blocking/ring-slide-4.jpg)
- [Slide 5 image](/res/adblockplus.org/static/gift-of-ad-blocking/ring-slide-5.jpg)
- [Slide 6 image](/res/adblockplus.org/static/gift-of-ad-blocking/ring-slide-6.jpg)
- [Slide 7 image](/res/adblockplus.org/static/gift-of-ad-blocking/ring-slide-7.jpg)
- [Slide 8 image](/res/adblockplus.org/static/gift-of-ad-blocking/ring-slide-8.jpg)

```
[Man searching for engagement ring on his laptop](ring-slide-1.jpg)
Ah, the perfect ring for a holiday proposal

[Man and woman browsing from their couch](ring-slide-2.jpg)
There’s no way she’ll expect this!

[Man looking at woman](ring-slide-3.jpg)
Perhaps it’s not the surprise you thought it was

[Woman showing man something on her laptop](ring-slide-4.jpg)
Perhaps it’s not the surprise you thought it was

[Couple on a couch](ring-slide-5.jpg)
Uh-oh. Ads have given the holiday game away

[Woman looking at her laptop scree at engagement rings](ring-slide-6.jpg)
It’s too late now!

[Couple fighting over laptop](ring-slide-7.jpg)
You weren’t meant to see that!

[Couple on couch next to Christmas tree](ring-slide-8.jpg)
Surprise…!?!
```

**Note:** Slide 3 and 4 have the same message under the image.

###### The kitchen

Asset(s):

- [Slide 1 image](/res/adblockplus.org/static/gift-of-ad-blocking/kitchen-slide-1.jpg)
- [Slide 2 image](/res/adblockplus.org/static/gift-of-ad-blocking/kitchen-slide-2.jpg)
- [Slide 3 image](/res/adblockplus.org/static/gift-of-ad-blocking/kitchen-slide-3.jpg)
- [Slide 4 image](/res/adblockplus.org/static/gift-of-ad-blocking/kitchen-slide-4.jpg)
- [Slide 5 image](/res/adblockplus.org/static/gift-of-ad-blocking/kitchen-slide-5.jpg)
- [Slide 6 image](/res/adblockplus.org/static/gift-of-ad-blocking/kitchen-slide-6.jpg)
- [Slide 7 image](/res/adblockplus.org/static/gift-of-ad-blocking/kitchen-slide-7.jpg)

```
[Man trying holiday recipe from his laptop](kitchen-slide-1.jpg)
Making something special for the holiday

[Man adding milk while cooking his holiday recipe](kitchen-slide-2.jpg)
It’s a new recipe

[Man mixing ingredients for Christmas cookies](kitchen-slide-3.jpg)
So far, so good

[Man spreads cookie dough and looks very pleased with himself](kitchen-slide-4.jpg)
Maybe I should check the recipe?

[Man trying to type on his laptop with dirty hands from cooking](kitchen-slide-5.jpg)
A badly-timed ad appears!

[Man with Santa hat looking at his laptop while cooking](kitchen-slide-6.jpg)
And the cookies are ruined...

[Man in Santa hat disappointed by cookies being ruined](kitchen-slide-7.jpg)
Give the gift of ad blocking
```

#### Sharing is caring

Beneath the tab content but still as part of the [photo story section](#photo-story-section), the user has sharing buttons so he may spread the word about the campaign. There's also a button that leads users to the [homepage](/spec/adblockplus.org/pages/homepage.md), where they can download the product.

Asset(s):

- Facebook icon
  - [SVG](/res/adblockplus.org/static/icon-facebook-white.svg)
  - [PNG](/res/adblockplus.org/static/icon-facebook-white.png)
  - [PNG@2x](/res/adblockplus.org/static/icon-facebook-white@2x.png)
- Twitter icon
  - [SVG](/res/adblockplus.org/static/icon-twitter-white.svg)
  - [PNG](/res/adblockplus.org/static/icon-twitter-white.png)
  - [PNG@2x](/res/adblockplus.org/static/icon-twitter-white@2x.png)

The sharing message and the [logic behind the social media icons](#social-sharing-2-click-solution) is the same as we have it in the hero section.

##### Content

```
Share this story with someone you care for [Share on Facebook](icon-facebook-white.svg) [Share on Twitter](icon-twitter-white.svg)

[Get the gift of ad blocking](index)
```

### Blog posts section

Beneath the [photo story section](#photo-story-section) we promote blog posts we've wrote on Medium.

#### Functionality

By default, on desktop, three images are displayed. When hovering an image, an overlay is displayed on top of it and the user can see an intro title as well as a `Read more` button that invites him to read more about the topic.

On tablets and mobile devices, the user can see both the image as well as the intro title with the `Read more` button.

[See demo on tablets >](/res/adblockplus.org/screenshots/goab-blog-post-section-tablets.png)

[See demo on mobiles >](/res/adblockplus.org/screenshots/goab-blog-post-section-mobiles.png)

**Note:** Mentioned _demo_ above (instead of _preview_) as the screen captures are strictly for demo purposes to explain how the content will be displayed for the user. The demo does not include final images or content. Please use the images below and the corresponding content for each content block.

#### Content

Asset(s):

- [Blog post image - parent](/res/adblockplus.org/static/gift-of-ad-blocking/blog-post-parent.jpg)
- [Blog post image - cookies](/res/adblockplus.org/static/gift-of-ad-blocking/blog-post-baking.jpg)
- [Blog post image - engagement](/res/adblockplus.org/static/gift-of-ad-blocking/blog-post-engagement.jpg)

##### Blog post - parent

```
Find out how one mother stopped worrying

[Read more](https://medium.com/the-gift-of-ad-blocking/confessions-of-an-overprotective-mother-bddd39614b6f?utm_source=medium&utm_medium=blog&utm_campaign=goab&utm_term=overprotectivemum)
```

##### Blog post - cookies

```
Get the recipe for an ad free holiday season (and cookies)

[Read more](https://medium.com/the-gift-of-ad-blocking/the-bake-off-or-salty-double-chocolate-cookies-to-save-christmas-8ff146ac3ecc?utm_source=medium&utm_medium=blog&utm_campaign=goab&utm_term=cookies)
```

##### Blog post - engagement

```
Find out how ads made the holidays surprising

[Read more](https://medium.com/the-gift-of-ad-blocking/how-re-targeted-ads-ruined-my-christmas-proposal-dc3018d68d1b?utm_source=medium&utm_medium=blog&utm_campaign=goab&utm_term=proposal)
```

### Footer

The page contains an modified version of the footer. The only change is the link to the Privacy Policy page as we have to create a new one specifically for this campaign and we don't want to include the specific adjustments in the main Privacy Policy on ABP.org.

[See spec for campaign-specific Privacy Policy page >](/spec/adblockplus.org/pages/privacy-goab.md)

[See modified footer >](/spec/adblockplus.org/includes/footer-goab.md)