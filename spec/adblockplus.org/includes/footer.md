# Footer

## Preview

- [Large screen](/res/adblockplus.org/screenshots/footer-large.png)
- [Medium screen](/res/adblockplus.org/screenshots/footer-medium.png)
- [Small screen](/res/adblockplus.org/screenshots/footer-small.png)

## Functionality

The `Resources`, `Community`, `Development`, and `Follow Us On` lists are displayed in one row of 4 columns on large screens and 2 rows of 2 columns on medium size screens.

The `Resources`, `Community`, and `Development` lists are hidden on small screens.

The copyright notice and legal links are displayed in one row of two columns on large screens and two rows on medium and small size screens.

## Contents

### `Resources`

```
- [Acceptable Ads](acceptable-ads)
- [Documentation](documentation)
- [For admins](deployments)
```

### `Community`

```
- [Announcements](releases)
- [Blog](https://adblockplus.org/blog/)
- [Forum](https://adblockplus.org/forum/)
- [Development builds](development-builds)
```

### `Development`

```
- [Source code](source)
- [Tools](tools)
- [Jobs](https://eyeo.com/jobs/)
```

### `Follow Us On`

| Icon | Alt text | Channel |
| :-- | :-- | :-- |
| [Twitter](/res/adblockplus.org/static//twitter-icon.png) | `Twitter` | https://twitter.com/AdblockPlus |
| [Youtube](/res/adblockplus.org/static//youtube-icon.png) | `Youtube` | https://www.youtube.com/user/AdblockPlusOfficial |
| [Facebook](/res/adblockplus.org/static//facebook-icon.png) | `Facebook` | https://www.facebook.com/adblockplus |
| [Instagram](/res/adblockplus.org/static//instagram-icon.png) | `Instagram` | https://www.instagram.com/adblockplus/ |

### Copyright notice

`Copyright © 2018 All rights reserved. Adblock Plus® is a registered trademark of eyeo GmbH.`

### Legal links

```
- [Terms of use](terms)
- [Privacy policy](privacy)
- [Imprint](impressum)
```
